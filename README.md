Задание:

С помощью create-react-app создать новое приложение под названием swapi-films, которое будет показывать список фильмов
из вселенной Star Wars.

Приложение должно работать следующим образом:

При старте приложения должен быть отправлен GET запрос по адресу https://ajax.test-danit.com/api/swapi/films для
получения списка фильмов серии
Пока список фильмов загружается с сервера, показывать пользователю текст Loading... (или по желанию CSS анимацию
загрузки)
После того, как фильмы будут получены с сервера, необходимо показать на экране пронумерованный список, каждый элемент
которого должен включать название фильма и кнопку "Детальнее"
При нажатии на кнопку "Детальнее", кнопка должна исчезать, а под названием фильма должны появиться два новых поля (
каждое с новой строки) - "Episode ID" (свойство episodeId) и "Opening crawl" (свойство openingCrawl)
Необязательное задание продвинутой сложности:

При нажатии на кнопку "Детальнее", показывать также еще одно поле - "Characters". Это должен быть список имен
персонажей, которые участвовали в данной серии.

Для получения имен персонажей, необходимо отправить несколько новых запросов на сервер (по одному на каждого персонажа).
Ссылки запросов на сервер для получения подробного описания каждого из персонажей находятся в поле characters фильма.

Пока имена персонажей загружаются с сервера, показывать в поле "Characters" текст Loading... или анимацию загрузки.
Необходимо показать имена персонажей, только когда все из них будут загружены.

======
ROUTES:

Задание:

Подключить библиотеку React Router
При заходе на главную страницу (/), пользователь должен автоматически перенаправляться на страницу /films
В приложении должно быть две рабочих страницы:
/films, на которой показывается список фильмов и кнопка "Детальнее". При нажатии на кнопку, пользователь перенаправляется на страницу конкретного фильма
/films/${filmId}, на которой показывается дополнительнвя информация про фильм, которая раньше показывалась под его названием при нажатии кнопки. 
Также на данной странице должна быть кнопка "Назад", которая возвращает пользователя обратно на страницу со списком всех фильмов
Необязательное задание продвинутой сложности:

Добавить страницу 404, которая будет показываться на всех неизвестных роутах.




# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes.\
You may also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more
information.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can't go back!**

If you aren't satisfied with the build tool and configuration choices, you can `eject` at any time. This command will
remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right
into your project so you have full control over them. All of the commands except `eject` will still work, but they will
point to the copied scripts so you can tweak them. At this point you're on your own.

You don't have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you
shouldn't feel obligated to use this feature. However we understand that this tool wouldn't be useful if you couldn't
customize it when you are ready for it.

## Learn More

You can learn more in
the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

### Code Splitting

This section has moved
here: [https://facebook.github.io/create-react-app/docs/code-splitting](https://facebook.github.io/create-react-app/docs/code-splitting)

### Analyzing the Bundle Size

This section has moved
here: [https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size](https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size)

### Making a Progressive Web App

This section has moved
here: [https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app](https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app)

### Advanced Configuration

This section has moved
here: [https://facebook.github.io/create-react-app/docs/advanced-configuration](https://facebook.github.io/create-react-app/docs/advanced-configuration)

### Deployment

This section has moved
here: [https://facebook.github.io/create-react-app/docs/deployment](https://facebook.github.io/create-react-app/docs/deployment)

### `npm run build` fails to minify

This section has moved
here: [https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify](https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify)
