import React from 'react';
import { useEffect, useState } from 'react';
import { minLoadTime } from '../../componets/loader/minLoadTime';
import FilmList from '../../componets/filmList/filmList';
import { Loader } from '../../componets/loader/loader';

const MovieList = () => {

  const [films, setFilms] = useState([]);
  const [isLoading, setIsLoading] = useState(true);

  // console.log(films);
  useEffect(
    () => {
      Promise.all([
        fetch('https://ajax.test-danit.com/api/swapi/films'),
        minLoadTime(1000)
      ])
        .then(res => res[0].json())
        .then(data => {
          setFilms(data);
          setIsLoading(false);
        });
    }, []
  );

  return (
    <>
      {isLoading ? <Loader/> :

        <FilmList films={films}/>
        // <FilmList films={films}/>
        // <FilmList films={films}/>
        // <FilmList films={films}/>
        // <footer />
      }
    </>
  );
};

export default MovieList;
