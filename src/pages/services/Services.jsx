import React, { useContext } from 'react';
import { Link, Outlet } from 'react-router-dom';

const Services = () => {
  // const { user } = useContext();

  return (
    <div>
      {/*<header></header>*/}
      <Link to="aboutus">
        services
      </Link>
      <div>
        Lorem ipsum dolor sit amet, consectetur adipisicing elit.
        Asperiores dolorum, laboriosam magnam nobis optio quasi quod temporibus?
        Aliquam commodi corporis cum, cumque eaque est ex fuga,
        repudiandae suscipit totam vitae!
      </div>
      <Outlet/>
      <div>Hello World</div>
      {/*<footer></footer>*/}
    </div>
  );
};

export default Services;
