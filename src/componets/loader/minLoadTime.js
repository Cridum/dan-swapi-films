export const minLoadTime = ms => new Promise(resolve => setTimeout(() => resolve() , ms))
