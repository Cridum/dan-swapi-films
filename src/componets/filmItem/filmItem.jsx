import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import './filmitem.css';

const FilmItem = ({ film }) => {
  return (
    <div>
      <p>{film.name}</p>
      <Link to={`/films/${film.id}/edit-user-page`}>FILM DETAILS</Link>
      {/*<Link to={`/films/123123`}>FILM DETAILS</Link>*/}

    </div>
  );
};

FilmItem.propTypes = {
  film: PropTypes.shape({
    episodeId: PropTypes.number,
  })
};

export default FilmItem;
