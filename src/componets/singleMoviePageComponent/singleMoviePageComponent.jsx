import React, { useEffect, useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import { minLoadTime } from '../loader/minLoadTime';
import { Loader } from '../loader/loader';

const SingleMoviePageComponent = () => {
  const { id } = useParams();
  const navigate = useNavigate();

  const [film, setFilm] = useState({});
  const [isLoading, setIsLoading] = useState(true);

  const goBack = () => navigate(-1);

  useEffect(() => {
    Promise.all([
      fetch(`https://ajax.test-danit.com/api/swapi/films/${id}`),
      minLoadTime(1000)
    ])
      .then(res => res[0].json())
      .then(data => {
        setFilm(data);
        setIsLoading(false);
      });
  }, []);

  return (
    <>
      {isLoading ? <Loader/> :
        <>
          <button onClick={goBack}>go back</button>
          <p>{film.name}</p>
          <p>{film.openingCrawl}</p>
          <p>{film.director}</p>
        </>
      }
    </>
  );
};

export default SingleMoviePageComponent;
