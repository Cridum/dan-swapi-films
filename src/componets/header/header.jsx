import React from 'react';
import { Link, NavLink } from 'react-router-dom';
import './header.css';
import CustomLink from '../customLink/customLink';
// import { routes } from '../../routes';

const Header = () => {

  const onActive = ({ isActive }) => isActive ? 'active-item' : '';
  return (
    <header>
      <nav className="nav">
        <ul className="nav-list">
          <li className="nav-item">
            <CustomLink to="/" activeClassName="link-active" newClassName="medium">
              home
            </CustomLink>
          </li>

          <li className="nav-item">

            <CustomLink to="/films" activeClassName="link-active" newClassName="large">
              films
            </CustomLink>
            <NavLink to="/films" className={({ isActive }) => isActive ? 'active-item' : ''}>

            </NavLink>

          </li>

          <li className="nav-item">
            <NavLink to="/services/aboutus" className={({ isActive }) => isActive ? 'active-item' : ''}>
              Outlet
            </NavLink>

          </li>
          <li className="nav-item">
            <NavLink to="/services" className={({ isActive }) => isActive ? 'active-item' : ''}>
              Services
            </NavLink>

          </li>
          <li className="nav-item">
            <a href="https://www.google.com.ua/?gws_rd=ssl" className="">
              link to google
            </a>
          </li>
        </ul>
      </nav>
    </header>
  );
};

export default Header;
