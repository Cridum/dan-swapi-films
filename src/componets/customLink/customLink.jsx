import { NavLink } from 'react-router-dom';

import './customLink.css';

const CustomLink = ({ children, to, activeClassName, newClassName }) => {

  return (
    <NavLink to={to} className={({ isActive }) => isActive ? `${activeClassName}` : `${newClassName}`}>
      {children}
    </NavLink>
  );
};

export default CustomLink;
