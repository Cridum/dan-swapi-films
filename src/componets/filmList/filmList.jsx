import React from 'react';
import FilmItemI from '../filmItem/filmItem';

import './filmlist.css';

const FilmList = ({ films }) => {
  return (
    <div>
      {films.map(el => <FilmItemI film={el} key={el.id}/>)}
    </div>
  );
};

export default FilmList;
