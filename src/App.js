import { useEffect, useState } from 'react';
import { Route, Routes, Navigate } from 'react-router-dom';

import Header from './componets/header/header';
import ProtectedRoute from './componets/protectedRoute/protectedRoute';

import MoviesListPage from './pages/movieListPage/MoviesListPage';
import SingleMoviePage from './pages/singleMoviePage/SingleMoviePage';
import Error404Page from './pages/errorPage/Error404Page';
import HomePage from './pages/homePage/HomePage';
import Services from './pages/services/Services';

import './App.css';

function App () {

  const login = () => {
    localStorage.setItem('user', 'userTokenId');
  };

  const logout = () => {
    localStorage.removeItem('user');
  };

  return (
    <main className="App">
      <Header/>
      <button type="button" onClick={login}>Log in</button>
      <button type="button" onClick={logout}>Log out</button>

      <h1>SW API Films</h1>
      <Routes>
        <Route path="/" element={<HomePage/>}/>
        <Route
          path="/services/"
          element={
            <ProtectedRoute isAuthenticated={!!localStorage.getItem('user')}>
              <Services/>
            </ProtectedRoute>
          }>
          <Route path="aboutus" element={<h1>About us</h1>}/>
        </Route>
        <Route path="/" element={<Navigate to="/films"/>}/>
        <Route path="/films" element={<MoviesListPage/>}/>

        {/*<Route path="films/:source" element={<SingleMoviePage/>}/>*/}
        <Route path="films/:id" element={<SingleMoviePage/>}/>
        {/*<Route path="films/search?name=filter&price" element={<SingleMoviePage/>}/>*/}
        {/*In single movie page => const {id} = useParams()*/}
        <Route path="*" element={<Error404Page/>}/>
      </Routes>
    </main>
  );
}

export default App;
